# Kali Linux WiFi Scripts #

![](https://img.shields.io/badge/wifi__scripts-bash-green.svg?style=flat-square) [![](https://img.shields.io/badge/License-WTFPL%202.0-lightgrey.svg?style=flat-square)](http://www.wtfpl.net/)

***
## Installation Instruction: ##

*These scripts work on Kali Linux 2.0 only*

**Download the zip file [here](http://bit.ly/1TsHFHO).**


1. unzip ruped24-wifi_scripts.xxx.zip
 
2. sudo chmod -R 755 ruped24-wifi_scripts.xxx
 
3. cd ruped24-wifi_scripts.xxx
 

```
#!shell

4. sudo ./menu2.sh
```

**List of tools:**
 
1. Change Mac Address
2. Crack Handshake
3. Crack Handshake Without Dictionary
4. Crack WiFi
5. Deauthorize Target Router
6. Generate List For Cracking
7. Mass Create APs With Generated Names
8. Mass Create APs With Preset Names
9. Jam All Nearby WiFi
10. Simple MITM Attack
11. Capture Raw Packets
12. Start|Stop Monitor Mode
13. Scan for LAN devices
14. Check Internet Adapters
15. Shutdown Computer Instantly
16. Change All Mac Addresses

**[Screenshot](https://drive.google.com/file/d/0B79r4wTVj-CZMEllRFVpUWh5TVE/view)**


Enjoy :wink: